package builders;

import cars.Car;
import cars.CarType;
import components.Engine;
import components.GPSNavigator;
import components.Transmission;
import components.TripComputer;

public class CarBuilder implements Builder {

    private CarType type;
    private int seats;
    private Engine engine;
    private Transmission transmission;
    private TripComputer tripcomputer;
    private GPSNavigator gpsnavigator;

    @Override
    public void setCarType(CarType  type){
        this.type = type;
    };

    @Override
    public void setSeats(int seats){
        this.seats = seats;
    }

    @Override
    public void setEngine(Engine  engine){
        this.engine = engine; 
    };
    

    @Override
    public void setTransmission(Transmission transmission){
        this.transmission = transmission;
    }   
    
    
    @Override
    public void setTripComputer(TripComputer tripcomputer){
        this.tripcomputer = tripcomputer;
    }
    
    
    @Override
    public void setGPSNavigator(GPSNavigator gpsnavigator){
        this.gpsnavigator = gpsnavigator;
    }

    public Car getResult(){
        return new Car(type, seats, engine, transmission, tripcomputer, gpsnavigator);
    }



}
