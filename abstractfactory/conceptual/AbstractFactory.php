<?php

namespace DesignPatterns\AbstractFactory\Conceptual;


interface AbstractFactory
{
    public function createProductA(): AbstractProductA;

    public function createProductB(): AbstractProductB;
}
