<?php


namespace DesignPatterns\AbstractFactory\Conceptual;

interface AbstractProductB
{
    public function usefulFunctionB(): string;

    public function anotherUsefulFunctionB(AbstractProductA $collaborator): string;
}
