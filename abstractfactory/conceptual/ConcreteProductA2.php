<?php

namespace DesignPatterns\AbstractFactory\Conceptual;


class ConcreteProductA2 implements AbstractProductA
{

    public function usefulFunctionA(): string
    {

        return  "The resut of the product A2";
    }
}
