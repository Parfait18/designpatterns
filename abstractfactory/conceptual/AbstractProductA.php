<?php

namespace DesignPatterns\AbstractFactory\Conceptual;

interface AbstractProductA
{

    public function usefulFunctionA(): string;
}
