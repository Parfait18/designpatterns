package  factories;

import afbuttons.Button;
import checkboxes.Checkbox;

public interface GUIFactory {

	Button createButton();

	Checkbox createCheckbox();

}
