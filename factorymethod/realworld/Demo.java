
import  factory.Dialog;
import  factory.HtmlDialog;
import  factory.WindowsDialog;

public class Demo {
    public static Dialog dialog;

    private static void main(String[] args) {
        configure();
        runBusinessLogic();
    }

    static void configure() {

        if (System.getProperty("os.name").equals("Windows 10")) {
            dialog = new WindowsDialog();
        } else {
            dialog = new HtmlDialog();
        }

    }

    static void runBusinessLogic() {
        dialog.renderWindow();
    }
}
